
package passwordapp;
import javax.swing.JOptionPane;
import java.util.Arrays;

public class PasswordApp {

    public static void main(String[] args) {

        String texto, textoLong;
        int numero, numeropass, i;
        // Pedimos al usuario el tamaño del array
        texto = JOptionPane.showInputDialog("Tamaño del array de passwords");
        numero = Integer.parseInt(texto);
        // Creamos array de contraseñas de tipo Password (Array de objetos)
        Password passwords [] = new Password[numero];
        // Creamos un array de booleanos
        boolean fuerte [] = new boolean [numero];
        
        // Pedimos al usuario la longitud de las contraseñas
        textoLong = JOptionPane.showInputDialog("Longitud de las contraseñas");
        numeropass = Integer.parseInt(textoLong);
        
        // Bucle para rellenar los dos arrays
        for (i=0;i<numero;i++){
            // Para el array de contraseñas creamos un objeto para cada posicion
            // Al crear el objeto, el contructor crea automaticamente la contraseña por medio del metedo generarPassword
            passwords[i]= new Password(numeropass);
            //Para el array de booleanos usamos el metodo esFuerte
            fuerte[i]=passwords[i].esFuerte();
            
            }
        
        // Imprimimos ambos arrays 
        for (i=0;i<passwords.length;i++){
            System.out.print(passwords[i].getContrasena()+ " "+ fuerte[i]);
            System.out.println("");
        
        }
        }
            
        }
    


