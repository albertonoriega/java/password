package passwordapp;

import java.lang.Math;

public class Password {

    private final static int LONG_DEF = 8;
    public int longitud;
    public String contrasena;
    
    // Constructor vacio; Por defecto la longitud sera 8. Hacemos uso de la constante declaramos junto con las propiedades
    public Password() {
        this(LONG_DEF);
        // como la longitud de la contraseña toma el valor de la constante, ya tiene longitud, por lo que llama al segundo constructor para generar la constraseña
        // seria lo mismo que poner:
        //this.longitud= LONG_DEF;
        //this.contrasena= generarPassword(longitud);
    }

    // Constructor que le pasamos la longitud y genera la contraseña con el metodo generarPassword()
    public Password(int longitud) {

        this.longitud = longitud;

        this.contrasena = generarPassword(longitud);
    }

    // Metodo que te genera una contraseña pasandole la longitud por parametro
    public static String generarPassword(int longitud) {
        
        char pass[] = new char[longitud];
        // generamos un array de acaracteres con todos los carecteres que puedan ser incluidos en la contraseña
        final String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        for (int i = 0; i < longitud; i++) {
            // Generamos un numero aleatorio del 0 al 62
            int numero = (int)( Math.random() * 62);
            // Para cada posicion del array de caracteres generamos aleatoriamente un caracter 
            pass[i] = caracteres.charAt(numero);
        }
        // Convertimos el array de caracteres en un String
        String contrasena = String.valueOf(pass);
        return contrasena;
    }
     // Otra opcion 
    public String generaPassword (){
        String contrasena="";
        char minusculas,mayusculas,numeros;
        int eleccion;
        for (int i=0;i<this.longitud;i++){
        /*Generamos un numero aleatorio, según este elige si añadir una minúscula,
           mayúscula o numero*/
           eleccion=((int)Math.floor(Math.random()*3+1));
           if (eleccion==1){
               // Codigo ASCII del 97 al 123 => letras minusculas
                minusculas=(char)((int)Math.floor(Math.random()*(123-97)+97));
                contrasena+=minusculas;
            }else{
                if(eleccion==2){
                    // Codigo ASCII del 65 al 97 => letras mayusculas
                    mayusculas=(char)((int)Math.floor(Math.random()*(91-65)+65));
                    contrasena+=mayusculas;
                }else{
                    // Codigo ASCII del 48 al 58 => numeros
                    numeros=(char)((int)Math.floor(Math.random()*(58-48)+48));
                    contrasena+=numeros;
                }
            }
        }
        return contrasena;
    }

    // Getter de longitud
    public int getLongitud() {
        return longitud;
    }

    // Getter contraseña
    public String getContrasena() {
        return contrasena;
    }

    // Setter Longitud
    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    // Metodo que devuelve true si tiene mas de 2 mayus, 1 minus y 5 numeros
    public boolean esFuerte() {
        int mayus = 0, minus = 0, num = 0;
        for (int i = 0; i < this.longitud; i++) {
            // Si comparas un numero con un caracter, se pueden comparar porque se compara el caracter con el caracter de la posicion del numero comparada en el codigo ASCII
            if (this.contrasena.charAt(i) >= 65 && this.contrasena.charAt(i) <= 90) {
                mayus++;
            } else if (this.contrasena.charAt(i) >= 97 && this.contrasena.charAt(i) <= 122) {
                minus++;
            } else if (this.contrasena.charAt(i) >= 48 && this.contrasena.charAt(i) <= 57) {
                num++;
            }

        }
        // Si cumple las condiciones devolvemos true
        if (mayus > 2 && minus > 1 && num > 5) {
            return true;
        } else {
        return false;
        }
    }
}
